mkdir -p empty && cd empty
docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -t hc2-build -f ../Dockerfile .
cd - > /dev/null && rm -rf empty

# copying include directories to host 
rm -rf /home/mh/hot/fibaro-hc-docker/sysroot/usr
mkdir -p  /home/mh/hot/fibaro-hc-docker/sysroot/usr/local

docker run -it --rm -v /home/mh/hot/fibaro-hc-docker/sysroot:/sysroot hc2-build \
	cp -r /usr/include /sysroot/usr

docker run -it --rm -v /home/mh/hot/fibaro-hc-docker/sysroot:/sysroot hc2-build \
	cp -r /usr/local/include /sysroot/usr/local
