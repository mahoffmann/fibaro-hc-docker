FROM ubuntu:xenial


RUN apt-get update && apt-get install -y --no-install-recommends \
	software-properties-common \
	ca-certificates \
 && add-apt-repository ppa:ubuntu-toolchain-r/test 


RUN apt-get update \
 && apt-get install -y --no-install-recommends \
    	cmake \
    	make \
	git \
    	gcc-5 \
    	g++-5 \
	gdb \
	libhunspell-dev \
	liblua5.2-dev \
	libluabind-dev \ 
	openssl \
	libssl-dev \
	libzmqpp-dev \
	unixodbc-dev \
	vim \
        wget \
 && rm -rf /var/lib/apt/lists/* && rm -rf var/cache/apt/*.bin 

RUN update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 80 \
 && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 80 \
 && update-alternatives --install /usr/bin/cc cc /usr/bin/gcc 90 \
 && update-alternatives --set cc /usr/bin/gcc \
 && update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++ 90 \
 && update-alternatives --set c++ /usr/bin/g++ 

RUN wget http://downloads.sourceforge.net/project/boost/boost/1.59.0/boost_1_59_0.tar.gz \
  && tar xfz boost_1_59_0.tar.gz \
  && rm boost_1_59_0.tar.gz 
RUN cd boost_1_59_0 \
  && ./bootstrap.sh --with-libraries=program_options,system,thread,regex,filesystem --prefix=/usr \
  && ./b2 install \
  && cd - \
&& rm -rf boost_1_59_0

RUN git clone https://github.com/pocoproject/poco.git -b poco-1.4.6 \
 && cd poco \
 && bash ./configure --no-samples --no-tests --omit=Data/ODBC,Data/MySQL,PageCompiler,PageCompiler/File2Page,CppUnit \
 && make -j4 && make install

ARG UID=1000
ARG GID=1000

RUN groupadd -g $GID mh
RUN useradd  -m -u $UID -g $GID -s /bin/bash mh
USER mh
